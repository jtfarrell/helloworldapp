package com.james.helloWorld;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class HelloWorldREST {

    @RequestMapping(value = "/")
    public String home() {
        String response = "Hello World!!" + new Date();
        return response;
    }

    @RequestMapping(value = "/exception")
    public String exception() {
        String response = "";
        try {
            throw new Exception("Exception has occured....");
        } catch (Exception e) {
            response = e.getMessage();
        }
        return response;
    }
}
