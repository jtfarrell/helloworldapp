package com.james.helloWorld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import co.elastic.apm.attach.ElasticApmAttacher;

@SpringBootApplication
public class HelloWorldApplication {

	public static void main(String[] args) {
		ElasticApmAttacher.attach();
		SpringApplication.run(HelloWorldApplication.class, args);
	}

}
